#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) 
  {
    // this is child
    //printf("masuk download\n");
    char link[100];
  
    sprintf(link, "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");
  
    char *argv[] = {"wget", "-qO", "/home/arfirf/Documents/prak_sisop/modul3/hehe.zip", link, NULL};
    execv("/usr/bin/wget", argv);
  } 
  else 
  {
    // this is parent
  
    while ((wait(&status)) > 0);
  
    //printf("masuk unzip\n");
    char *args[] = {"unzip", "-q" ,"/home/arfirf/Documents/prak_sisop/modul3/hehe.zip", "-d", "/home/arfirf/Documents/prak_sisop/modul3",NULL};
    execv("/usr/bin/unzip", args);
  }
}
