#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <pthread.h>
#include <dirent.h>

#define NUM_THREADS 7 // jumlah thread yang akan dibuat

char *ext[7] = {"jpg", "txt", "js", "py", "png", "emc", "xyz"};
pthread_t th[500];

FILE *prog_log;
char timestamp[100];
time_t timer;

void *createFolder(void *args)
{
  time(&timer);
  strftime(timestamp, 100, "%d-%m-%Y %X", localtime(&timer));

  char folder_name[100];
  sprintf(folder_name, "/home/arfirf/Documents/prak_sisop/modul3/categorized/%s", ext[(int)args]);
  mkdir(folder_name, 0777); // hak akses 777
  
  printf("Thread %ld: Created folder %s\n", (long)args, folder_name);
  fprintf(prog_log, "%s MADE categorized/%s\n", timestamp, ext[(int)args]);
  fflush(prog_log);
  pthread_exit(NULL);
}

void *listFiles(char *path) {

    int index;
    DIR *dir;
    struct dirent *entry;
    struct stat statbuf;
    char fullpath[1024];
    char filepath[1024];

    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
    
    	time(&timer);
        strftime(timestamp, 100, "%d-%m-%Y %X", localtime(&timer));
        
        if ((entry->d_name[0] == '.') || (entry->d_name[0] == '..'))
        {
          continue; 
        }
        
        //pthread_create(&th[index], NULL, move, (void *)&arg);
         
        snprintf(fullpath, sizeof(fullpath), "%s/%s", path, entry->d_name);
        lstat(fullpath, &statbuf);

        if (S_ISDIR(statbuf.st_mode)) {
            // listing file secara rekursif
            listFiles(fullpath);
        } else {
            // print path to file
            realpath(path, filepath);
            printf("%s/%s\n", filepath, entry->d_name);
            
            fprintf(prog_log, "%s ACCESSED %s/%s\n", timestamp, filepath, entry->d_name);
  	    fflush(prog_log);
        }
        
        index++;
        
    }
    closedir(dir);
}

int main()
{
  prog_log = fopen("log.txt", "w+");
  
  pid_t child;
  int status;
  
  child = fork();
  if (child < 0) {
  exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child == 0) // this is child
  {
    //char timestamp[100];
    //time_t timer;
    
    time(&timer);
    strftime(timestamp, 100, "%d-%m-%Y %X", localtime(&timer));
   
    fprintf(prog_log, "%s MADE categorized\n", timestamp);
    fflush(prog_log);
    
    char *argv[] = {"mkdir", "-p", "categorized", NULL};
    execv("/bin/mkdir", argv);
    
  } 
  else 
  {

    // this is parent
    
    while ((wait(&status)) > 0);
    printf("masuk parent setelah buat folder categorized\n");

    pthread_t threads[NUM_THREADS];
    int i;
    for (i = 0; i < NUM_THREADS; i++) 
    {
        printf("Creating thread %d\n", i);
        int thc = pthread_create(&threads[i], NULL, createFolder, (void *)i); // membuat thread
        if (thc) {
            printf("Error: Return code from pthread_create() is %d\n", thc);
            exit(-1);
        }
    }
    for (i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL); // menunggu semua thread selesai
    }
    //printf("All threads and folder creations have completed.\n");
    
    listFiles("/home/arfirf/Documents/prak_sisop/modul3/files");
  }
  
  
  
  fclose(prog_log);
  return 0;
}
