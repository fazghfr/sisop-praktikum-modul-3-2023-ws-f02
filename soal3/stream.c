#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/wait.h>
#include <json-c/json.h>
#include <fcntl.h>
#include <stdbool.h>
#include <semaphore.h>

#define KEY_VALUE 91
#define CHUNK_SIZE 1024
#define FILENAME "song-playlist.json"
#define PLFILE "playlist.txt"
#define MAX_BLOCKED_USERS 8
#define MAX_ALLOWRD_USERS 10


typedef struct message_buffer {
    long message_type;
    char message_text[1024];
    int msg_length;
    uid_t user_id;
}message_buffer;

message_buffer decrypt(char *cmd, char song[]);
message_buffer decrypt_rot13(char str[], int length);
message_buffer decrypt_base64(char str[], int length) ;
message_buffer decrypt_hex(char str[], int length);
message_buffer build_message(char *str, int type);
message_buffer play_song(char *song, int length);
message_buffer add_song(char *song, int length);

char* getJsonString(char filename[]);
int countLine(char filename[]);
void send_message(int msgid, message_buffer message, int type);
void processJson();
void list_pL();
void print_numbered_lines(char *filename);
void deleteFile(char *filename);

uid_t current_userid;
uid_t prev_userid;
sem_t sem;
bool isFull = false;
uid_t blocked_users[MAX_BLOCKED_USERS] = {-1};
uid_t allowed_users[MAX_ALLOWRD_USERS] = {-1};

bool isBlocked(uid_t id){
    bool is_blocked = false;
        for (int i = 0; i < MAX_BLOCKED_USERS; i++) {
            if (blocked_users[i] == id) {
                is_blocked = true;
                break;
            }
       }
    
    return is_blocked;
}

bool isAllowd(uid_t id){
    bool det = false;
        for (int i = 0; i < MAX_ALLOWRD_USERS; i++) {
            if (allowed_users[i] == id) {
                det = true;
                break;
            }
       }
    
    return det;
}

void addAllowrd(uid_t id){
        for (int i = 0; i < MAX_ALLOWRD_USERS; i++) {
            if (allowed_users[i] == -1) {
                allowed_users[i] = id;
                break;
            }
       }
    
    return;
}

void addBlocked(uid_t id){
        for (int i = 0; i < MAX_BLOCKED_USERS; i++) {
            if (blocked_users[i] == -1) {
                blocked_users[i] = id;
                break;
            }
       }
    
    return;
}

int main() {
    sem_init(&sem, 0, 2);
    int ret = 2;
    key_t key;
    int msgid;

    key = ftok("progfile", KEY_VALUE);

    msgid = msgget(key, 0666 | IPC_CREAT);

    message_buffer message;
    int count = 0;
    while (msgrcv(msgid, &message, sizeof(message), 1, IPC_NOWAIT) != -1) {
        message_buffer respond;
        if(count==0){
            prev_userid = -1;
        }else{
            prev_userid = current_userid;
        }

        respond.user_id = message.user_id;
        current_userid = respond.user_id;



        char *cmd = strtok(message.message_text, " ");
        count++;
        if(isBlocked(current_userid)){
            printf("OVERLOADED ID %d :", current_userid);
            respond = build_message("OVERLOADDED", 2);
        }

        else if(strcmp(cmd, "decrypt")==0){
            char *json_str = getJsonString(FILENAME);
            processJson(json_str);

            respond = build_message("Decryption Successful", 2);
        }

        else if(strcmp(cmd, "list")==0){
            list_pL();
            respond = build_message("List Process Succesful", 2);
        }

        else if(strcmp(cmd, "PLAY")==0){
            int bfore = -1;
            if(prev_userid != current_userid){
                sem_getvalue(&sem, &bfore);
                sem_trywait(&sem);
                sem_getvalue(&sem, &ret);
            }
            

            if (bfore == 0 && !isAllowd(current_userid)) { // full
                isFull = true;
                addBlocked(current_userid);
                printf("OVERLOADED ID %d :", current_userid);
                respond = build_message("OVERLOADED", 2);
            
            } else {
                cmd = strtok(NULL, "\"");
                if(cmd != NULL){
                    addAllowrd(current_userid);
                    printf("ALLOWED ID %d :", current_userid);
                    respond = play_song(cmd, strlen(cmd));
                }else{
                    respond = build_message("WRONG FORMAT", 2);
                }
            }
        }

        else if(strcmp(cmd, "ADD")==0){
            int bfore = -1;
            if(prev_userid != current_userid){
                sem_getvalue(&sem, &bfore);
                sem_trywait(&sem);
                sem_getvalue(&sem, &ret);
            }
            

            if (bfore == 0 && !isAllowd(current_userid)) { // full
                isFull = true;
                addBlocked(current_userid);
                printf("OVERLOADED ID %d :", current_userid);
                respond = build_message("OVERLOADED", 2);
            
            } else {
                cmd = strtok(NULL, "\"");
                if(cmd != NULL){
                    addAllowrd(current_userid);
                    printf("ALLOWED ID %d :", current_userid);
                    respond = add_song(cmd, strlen(cmd));
                }else{
                    respond = build_message("WRONG FORMAT", 2);
                }
            }
        }

        else {
            respond = build_message("UNKNOWN COMMAND", 2);
        }

        if(respond.message_type!=13){
            printf("%s\n", respond.message_text);
        }else{
            continue;
        }
    }

    // to destroy the message queue
    msgctl(msgid, IPC_RMID, NULL);
    return 0;
}


void delete_file(char *filename){
    pid_t pid = fork();
    if(pid == -1) {
        printf("Error forking process.\n");
        return;
    } else if(pid == 0) {
        char *args[] = {"rm", filename, NULL};
        execvp(args[0], args);
        printf("Error deleting file.\n");
        exit(1);
    } else {
        wait(NULL);
    }
}

int countLine(char filename[]){
    FILE *fp = fopen(filename, "r");
    if(fp == NULL) {
        printf("Error opening file.\n");
        return -1;
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int line_num = 1;

    while((read = getline(&line, &len, fp)) != -1) {
        line_num++;
    }

    fclose(fp);

    return line_num-1;
}

message_buffer add_song(char *song, int length){
    /*
    1.find out if the song is already exist or not
    2.add the song
    */    
    
    char cmd[512];
    sprintf(cmd, "grep \"%s\" %s >> %s", song, PLFILE, "temp.txt");
    system(cmd);

    if(countLine("temp.txt")!=0){
        delete_file("temp.txt");
        return build_message("SONG ALREADY ON PLAYLIST", 2);
    }
    delete_file("temp.txt");

    char cmd2[512];
    sprintf(cmd2, "echo %s >> %s", song, PLFILE);
    system(cmd2);

    char respon[512];
    sprintf(respon, "USER %d ADD %s", current_userid, song);
    return build_message(respon, 2);
}

void print_numbered_lines(char *filename) {
    FILE *fp = fopen(filename, "r");
    if(fp == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int line_num = 1;

    while((read = getline(&line, &len, fp)) != -1) {
        printf("%d. %s", line_num++, line);
    }

    fclose(fp);

    if(line) {
        free(line);
    }

    delete_file(filename);
}

message_buffer play_song(char *song, int length){
    char command[512];
    sprintf(command, "grep \"%s\" %s", song, PLFILE);

    FILE *p = popen(command, "r");
    if(p == NULL){
        printf("Error opening pipe with given command");
        return build_message("error openinng pipe", -1);
    }

    char *output = NULL;
    size_t output_size = 0;

    char buffer[1024];
    int count = 1; // add a count variable to keep track of song numbers
    while(fgets(buffer, sizeof(buffer), p)){
        char numbered_output[1024];
        sprintf(numbered_output, "%d. %s", count, buffer); // add song number to the output
        size_t buffer_len = strlen(numbered_output);
        output = realloc(output, output_size + buffer_len + 1);
        if(output == NULL){
            fclose(p);
            return build_message("out of memory", -1);
        }
        memcpy(output + output_size, numbered_output, buffer_len);
        output_size += buffer_len;
        count++; // increment the song count
    }

    fclose(p);

    if(output_size > 0 && output[output_size-1] == '\n'){
        output[output_size-1] = '\0';
        output_size--;
    }

    if(count-1 == 1){
        char *formatted_output = malloc(strlen(output) + strlen(song) + 32); // allocate new buffer
        sprintf(formatted_output, "USER %d IS PLAYING \"%s\"", current_userid, output + 2); // format new string, skipping song number
        return build_message(formatted_output, 2);

    } else if(count - 1 == 0){
        char *response = malloc(strlen(song) + 32); // allocate new buffer
        sprintf(response, "THERE IS NO SONG CONTAINING \"%s\"", song);
        return build_message(response, 2);
    }

    char header[96+strlen(song)];
    sprintf(header, "THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", count-1, song);

    printf("%s", header);
    char cmd[512];
    sprintf(cmd, "grep \"%s\" \"%s\" >> %s", song, PLFILE, "temp.txt");
    system(cmd);
    
    print_numbered_lines("temp.txt");
    return build_message("Default", 13);
}


void send_message(int msgid, message_buffer message, int type){
    msgsnd(msgid, &message, sizeof(message), type);
    printf("Data send is : %s \n", message.message_text);
}

void list_pL(){
    char command[512];
    sprintf(command, "sort -o %s %s", PLFILE, PLFILE);
    system(command);
}
message_buffer build_message(char *str, int type){
    message_buffer msg;
    if(type<0){
        strcpy(msg.message_text, " ");
        msg.msg_length = -1; 
        msg.message_type = type;

        return msg;
    }

    strcpy(msg.message_text, str);
    msg.msg_length = strlen(str); 
    msg.message_type = type;

    return msg;
}

message_buffer decrypt_rot13(char str[], int length){
    int i;
    for (i = 0; i<length ; i++) {
        char c = str[i];
        if (isalpha(c)) {
            if (isupper(c)) {
                str[i] = ((c - 'A' + 13) % 26) + 'A';
            } else {
                str[i] = ((c - 'a' + 13) % 26) + 'a';
            }
        }
    }

    return build_message(str, 2);
}

message_buffer decrypt_base64(char str[], int length){
    char command[1024];
    sprintf(command, "echo %s | base64 -d", str);

    FILE *p = popen(command, "r");
    if(p==NULL){
        printf("Error opening pipe with given command");
        return build_message("error openinng pipe", -1);
    }

    char output[length+1];
    fgets(output, sizeof(output), p);
    fclose(p);

    output[strcspn(output, "\n")] = '\0';
    return build_message(output, 2);
}

message_buffer decrypt_hex(char str[], int length){
    char command[1024];
    sprintf(command, "echo %s | hex -d", str);

    FILE *p = popen(command, "r");
    if(p==NULL){
        printf("Error opening pipe with given command");
        return build_message("error opening pipe", -1);
    }

    char output[length+1];
    fgets(output, sizeof(output), p);
    fclose(p);

    output[strcspn(output, "\n")] = '\0';
    return build_message(output, 2);
}

message_buffer decrypt(char *cmd, char song[]){
    if(strcmp(cmd, "rot13")==0){
        return decrypt_rot13(song, strlen(song));
    }
    else if(strcmp(cmd, "base64") == 0){
        return decrypt_base64(song, strlen(song));
    }
    else if(strcmp(cmd, "hex") == 0){
        return decrypt_hex(song, strlen(song));
    }
    
    return build_message("UNKNOWN COMMAND", 2);
}

char * getJsonString(char filename[]){
    FILE *fp = fopen(filename, "r");
    if (!fp) {
        fprintf(stderr, "Error: Could not open input file %s\n", filename);
        return NULL;
    }

    char chunk[CHUNK_SIZE + 1];
    char *json_str = NULL;
    size_t json_str_len = 0;
    struct json_tokener *tok = json_tokener_new();

    while (fgets(chunk, CHUNK_SIZE, fp) != NULL) {
        json_object *obj = json_tokener_parse_ex(tok, chunk, strlen(chunk));
        enum json_tokener_error jerr = json_tokener_get_error(tok);
        if (jerr != json_tokener_success && jerr != json_tokener_continue) {
            fprintf(stderr, "Error: %s\n", json_tokener_error_desc(jerr));
            json_tokener_free(tok);
            fclose(fp);
            return NULL;
        }

        if (obj != NULL) {
            json_str = realloc(json_str, json_str_len + strlen(json_object_to_json_string(obj)) + 1);
            strcat(json_str, json_object_to_json_string(obj));
            json_str_len += strlen(json_object_to_json_string(obj));
            json_object_put(obj);
        }
    }

    json_tokener_free(tok);
    fclose(fp);

    return json_str;
}

void processJson(char *json_str){
    struct json_object *json_array = json_tokener_parse(json_str);
    if (!json_array || json_object_get_type(json_array) != json_type_array) {
        fprintf(stderr, "Error: Invalid JSON array\n");
        free(json_str);
        return;
    }

    // Iterate over the JSON objects in the array and print their "method" and "song" values
    size_t array_len = json_object_array_length(json_array);
    for (size_t i = 0; i < array_len; i++) {
        json_object *json_obj = json_object_array_get_idx(json_array, i);
        json_object *method_obj, *song_obj;

        if (!json_object_object_get_ex(json_obj, "method", &method_obj) ||
            !json_object_object_get_ex(json_obj, "song", &song_obj)) {
            fprintf(stderr, "Error: Missing attribute in JSON object at index %zu\n", i);
            continue;
        }

        char const *method = json_object_get_string(method_obj);
        char const *song = json_object_get_string(song_obj);

        //process it
        message_buffer msg = decrypt(method, song);
        
        FILE *fp = fopen(PLFILE, "a");
        if (fp == NULL) {
            fprintf(stderr, "Error opening file\n");
            exit(EXIT_FAILURE);
        }
        fprintf(fp, "%s\n", msg.message_text);
        fclose(fp);
    }

    json_object_put(json_array);
    free(json_str);
}
