#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <semaphore.h>
#include <fcntl.h>

#define KEY_VALUE 91
#define MAX_TEXT 1024

typedef struct message_buffer {
    long message_type;
    char message_text[MAX_TEXT];
    int msg_lenth;
    uid_t user_id;
}message_buffer;

void send_message(int msgid, message_buffer message, int type);
message_buffer receive_message(int message_queue_id);

void toLowercase(char *str) {
    for (int i = 0; str[i]; i++) {
        str[i] = tolower(str[i]);
    }
}

int main() {

    key_t key;
    int msgid;
    char input[MAX_TEXT];

    key = ftok("progfile", KEY_VALUE);

    msgid = msgget(key, 0666 | IPC_CREAT);
    
    while (1) {
        message_buffer message;
        message.message_type = 1;
        printf("Enter Data (0 to exit): ");
        fgets(input, MAX_TEXT, stdin);
        if (strcmp(input, "0\n") == 0) break;

        char *first_word = malloc(sizeof(input));
        first_word = strcpy(first_word,input);
        first_word = strtok(first_word, " ");
        if(strcmp(first_word, "PLAY")!=0 && strcmp(first_word, "ADD")!=0){
            toLowercase(input);
        }
        
        input[strlen(input)-1] = '\0';
        // copy the message text to message buffer
        strncpy(message.message_text, input, MAX_TEXT);

        // send message to message queue
        send_message(msgid, message, 1);
    }
    return 0;
}

void send_message(int msgid, message_buffer message, int type){
    msgsnd(msgid, &message, sizeof(message), type);
    printf("Data send is : %s \n", message.message_text);
}