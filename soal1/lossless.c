#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

#define MAX_CHAR 256

#include <stdio.h>
#include <stdlib.h>

#define MAX_TREE_HT 50

char send[26];

struct MinHNode {
  char item;
  unsigned freq;
  struct MinHNode *left, *right;
};

/*
    implementasi algoritma direferensikan dari:
    https://www.programiz.com/dsa/huffman-coding

    dengan modifikasi
*/
struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHNode **array;
};

// Create nodes
struct MinHNode *newNode(char item, unsigned freq) {
  struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

  temp->left = temp->right = NULL;
  temp->item = item;
  temp->freq = freq;

  return temp;
}

// Create min heap
struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
  return minHeap;
}

// Function to swap
void swapMinHNode(struct MinHNode **a, struct MinHNode **b) {
  struct MinHNode *t = *a;
  *a = *b;
  *b = t;
}

// Heapify
void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}

// Check if size if 1
int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}

// Extract min
struct MinHNode *extractMin(struct MinHeap *minHeap) {
  struct MinHNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

// Insertion function
void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHNode *root) {
  return !(root->left) && !(root->right);
}

struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(item[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size) {
  struct MinHNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}

void printHCodes(struct MinHNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->item)!=NULL) {
    printArray(arr, top, root->item);
  }
}

// Wrapper function
void HuffmanCodes(char item[], int freq[], int size) {
  struct MinHNode *root = buildHuffmanTree(item, freq, size);

  int arr[MAX_TREE_HT], top = 0;

  printHCodes(root, arr, top);
}

// Print the array
void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    // Check if file opened successfully
    if (fp == NULL) {
        printf("Error opening file\n");
        return 1;
    }

  int i;

  fprintf(fp, "%c", c);
  for (i = 0; i < n; ++i){
    // Append string to file
    fprintf(fp, "%d", arr[i]);
  }

  fprintf(fp, "%c", '\n');
  fclose(fp);
}

int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}

int findLength(char c){
    char line[512];
    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
    if (strchr(line, c) != NULL) {
        //translate
        return strlen(line) - 1;
    }
}


    fclose(fp);
    return -1;
}

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

    if (fp == NULL || fs == NULL || fc == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char line[512];
    char c;
    int count = 0;
    while ((c = fgetc(fc)) != EOF) {
        fseek(fs, 0, SEEK_SET);
        c = toupper(c);
        while (fgets(line, 512, fs) != NULL) {
            if (line[0] == c) {
                char *sub = malloc(strlen(line));
                strcpy(sub, line + 1);
                count += strlen(sub)-1;
                sub[strlen(sub) - 1] = '\0';
                fwrite(sub, strlen(sub), 1, fp);
                free(sub);
                break;
            }
        }
    }


    fclose(fp);
    fclose(fs);
    fclose(fc);
    return count;
}

int main() {
    int fd1[2], fd2[2];
    pid_t pid;

    // Buat dua pipes
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // Buat child process
    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) { // child process
        char *arr = malloc(sizeof(char) * 26);
        int *arrint = malloc(sizeof(int) * 26);
        char *sent = malloc(sizeof(char)*26);
        close(fd1[1]);
        read(fd1[0], arr, sizeof(char) * 26);
        read(fd1[0], arrint, sizeof(int) * 26);
        read(fd1[0], sent, 26);
        close(fd1[0]);

        strcpy(send, sent);

        close(fd1[0]);
        HuffmanCodes(arr, arrint, sizeof(char)*26);

        close(fd2[0]);
        int countLine = count_line("temp.txt");
        write(fd2[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
            if(fp==NULL){
                perror("fail to open");
                exit(1);
            }

            char line[512];
            while(fgets(line, 512, fp)!= NULL){
                write(fd2[1], &line, sizeof(line));
            }
        close(fd2[1]);
    } else { // parent process
        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0}; 
        char alpha[26] = {'\0'}; 
        int idx = 0; 
        while ((c = fgetc(fp)) != EOF) {
            if (isalpha(c)) { // check if character is alphabetic
                c = toupper(c); // convert to uppercase
                int pos = c - 'A'; // get position in frequency array
                freq[pos]++; // increment frequency of corresponding letter
                if (freq[pos] == 1) { // if first occurrence of letter
                    alpha[idx] = c; // add letter to alphabet array
                    idx++; // increment index for alphabet array
                }
            }
        }

        char *arr = malloc(sizeof(char) * 26);
        int *arrfr = malloc(sizeof(int) * 26);

        // Print frequency of each letter
        int j = 0;
        for (int i = 0; i < 26; i++) {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if(freq[i]!=0){
                send[j] = alpha[i];
                j++;
            }
        }

        // Print alphabet list
        for (int i = 0; i < 26; i++) {
            arr[i] = alpha[i];
        }

        // Close file
        fclose(fp);

        close(fd1[0]);
        write(fd1[1], arr, sizeof(char) * 26);
        write(fd1[1], arrfr, sizeof(int) * 26);
        write(fd1[1], send, sizeof(send));
        close(fd1[1]);

        close(fd2[1]);
        int n;
        read(fd2[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++) {
            read(fd2[0], coded[i], sizeof(char) * 512);
        }

        close(fd2[0]);
        int number = 0;
        for(int i = 0; i<26; i++){
            int calc;
            if(freq[i]!=0){
                calc = freq[i]*8;
                number += calc;
            }
        }

        printf("%d BIT SEBELUM HUFMAN\n", number);

        number = translate("translated.txt");;
        printf("%d AFTER HUFMAN\n", number);

        system("rm temp.txt");
    }


    return 0;
}
