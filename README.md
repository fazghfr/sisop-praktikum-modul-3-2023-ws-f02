# Sistem Operasi F - Laporan Resmi Shift 3
Kelas Sistem Operasi F - Kelompok F02

## Anggota Kelompok :
- Salsabila Fatma Aripa (5025211057)
- Arfi Raushani Fikra (5025211084)
- Ahmad Fauzan Alghifari (5025211091)

### Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan **Algoritma Huffman** untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

**a.** Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

**b** Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

**c.** Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

**d.** Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

**e.** Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

**Catatan:**
- **Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya, Agar lebih mudah, ubah semua huruf kecil ke huruf kapital**

#### Penyelesaian
Membuat file **lossless.c** dengan menggunkan **Algoritma Huffman**

Pertama, membuat konstanta yang menentukan jumlah karakter maksimum dan tinggi maksimum pohon Huffman yang dibangun. Selain itu, terdapat deklarasi struktur data untuk mengimplementasikan algoritma Huffman coding. Struktur data ini terdiri dari dua bagian, yaitu struktur MinHNode dan MinHeap.
```c
#define MAX_CHAR 256
#define MAX_TREE_HT 50

char send[26];
struct MinHNode {
  char item;
  unsigned freq;
  struct MinHNode *left, *right;
};
struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHNode **array;
};
```
Kedua,  implementasi dari dua fungsi yang digunakan dalam algoritma Huffman. Fungsi pertama, newNode, berfungsi untuk membuat node baru dengan karakter item dan frekuensi freq. Fungsi ini mengalokasikan memori untuk node baru dengan menggunakan fungsi malloc dan menginisialisasi nilai left dan right menjadi NULL, serta mengisi nilai item dan freq sesuai dengan parameter yang diberikan.

Fungsi kedua, createMinH, berfungsi untuk membuat sebuah min heap dengan kapasitas capacity. Fungsi ini juga mengalokasikan memori untuk min heap baru dengan menggunakan fungsi malloc. Kemudian, ukuran min heap diatur ke nilai awal 0, kapasitas diatur sesuai dengan parameter yang diberikan, dan array min heap diinisialisasi dengan menggunakan fungsi malloc untuk mengalokasikan memori untuk array dengan kapasitas sebesar capacity.
```c
// Create nodes
struct MinHNode *newNode(char item, unsigned freq) {
  struct MinHNode *temp = (struct MinHNode *)malloc(sizeof(struct MinHNode));

  temp->left = temp->right = NULL;
  temp->item = item;
  temp->freq = freq;

  return temp;
}

// Create min heap
struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHNode **)malloc(minHeap->capacity * sizeof(struct MinHNode *));
  return minHeap;
}
```
Ketiga, implementasi dari beberapa fungsi yang terkait dengan struktur data min heap. Fungsi swapMinHNode digunakan untuk menukar posisi dua node dalam min heap. Fungsi minHeapify digunakan untuk menjaga properti min heap ketika sebuah node dihapus atau dimasukkan ke dalam min heap. Fungsi ini membandingkan frekuensi dari node di indeks idx dengan frekuensi dari anak kiri dan kanannya, lalu memperbaiki posisi node jika diperlukan untuk menjaga properti min heap. Indeks anak kiri dari sebuah node dapat dihitung dengan rumus `2idx+1` dan indeks anak kanannya dengan rumus `2idx+2`
```c
// Function to swap
void swapMinHNode(struct MinHNode **a, struct MinHNode **b) {
  struct MinHNode *t = *a;
  *a = *b;
  *b = t;
}

// Heapify
void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}
    // Check if size if 1
    int checkSizeOne(struct MinHeap *minHeap) {
    return (minHeap->size == 1);
}
```
Keempat,  implementasi dari operasi pada sebuah heap minimum (MinHeap), yang digunakan dalam algoritma Huffman coding. Lebih spesifiknya, di atas terdapat tiga fungsi: extractMin, insertMinHeap, dan buildMinHeap, serta satu fungsi bantuan isLeaf.

Fungsi extractMin menghapus elemen dengan frekuensi terkecil dari MinHeap dan mengembalikan alamat node-nya. Fungsi ini mengambil alamat array dari MinHeap, memindahkan elemen terakhir ke posisi root, mengurangi ukuran heap, kemudian memanggil minHeapify pada root untuk menjaga sifat heap minimum.

Fungsi insertMinHeap menambahkan node baru ke dalam MinHeap dan memastikan bahwa sifat heap minimum tetap terjaga dengan memindahkan node baru ke atas hingga semua parent-nya memiliki frekuensi yang lebih kecil.

Fungsi buildMinHeap membangun MinHeap dari array node yang sudah ada dengan memanggil minHeapify pada setiap node yang bukan leaf secara terbalik dari posisi terakhir hingga root.

Fungsi isLeaf digunakan untuk memeriksa apakah sebuah node merupakan leaf (tidak memiliki anak). Jika node tersebut tidak memiliki anak kiri dan kanan, maka node tersebut adalah leaf dan akan digunakan sebagai basis dalam pembangunan tree oleh algoritma Huffman.
```c
// Extract min
struct MinHNode *extractMin(struct MinHeap *minHeap) {
  struct MinHNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}

// Insertion function
void insertMinHeap(struct MinHeap *minHeap, struct MinHNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}

int isLeaf(struct MinHNode *root) {
  return !(root->left) && !(root->right);
}
```
Kelima, implementasi createAndBuildMinHeap: Fungsi ini digunakan untuk membuat min heap yang berisi node-node yang berisi item dan frekuensinya. Fungsi ini menerima 3 parameter, yaitu item yang berisi karakter/kata yang ingin dikompresi, freq yang berisi frekuensi masing-masing karakter/kata, dan size yang merupakan jumlah karakter/kata yang ingin dikompresi. Fungsi ini akan membuat min heap dengan ukuran size, kemudian mengisi min heap dengan node-node yang berisi item dan freq, lalu membangun heap.

selanjutnya implementasi, buildHuffmanTree: Fungsi ini digunakan untuk membangun pohon Huffman dari min heap yang telah dibuat. Fungsi ini menerima 3 parameter, yaitu item yang berisi karakter/kata yang ingin dikompresi, freq yang berisi frekuensi masing-masing karakter/kata, dan size yang merupakan jumlah karakter/kata yang ingin dikompresi. Fungsi ini akan memanggil createAndBuildMinHeap untuk membuat dan membangun min heap, kemudian melakukan loop hingga ukuran min heap menjadi 1. Pada setiap iterasi, fungsi akan mengeluarkan 2 node dengan frekuensi terkecil dari min heap, lalu membuat sebuah node baru sebagai parent dari kedua node tersebut, dan menambahkan node parent ke dalam min heap. Setelah loop selesai, fungsi akan mengeluarkan node yang tersisa di dalam min heap sebagai root dari pohon Huffman.
```c
struct MinHeap *createAndBuildMinHeap(char item[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(item[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}

struct MinHNode *buildHuffmanTree(char item[], int freq[], int size) {
  struct MinHNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(item, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}
````
Keenam, Fungsi printHCodes adalah untuk mencetak kode Huffman dari setiap karakter pada pohon Huffman yang dibangun. Fungsi ini menerima tiga parameter yaitu root yang merupakan pointer ke akar pohon Huffman, arr yang merupakan array untuk menyimpan kode Huffman dan top yang merepresentasikan jumlah bit pada kode Huffman.

Fungsi HuffmanCodes merupakan fungsi wrapper yang memanggil fungsi buildHuffmanTree untuk membangun pohon Huffman dari karakter dan frekuensinya. Selanjutnya, fungsi ini membuat array arr dan variabel top, dan memanggil fungsi printHCodes dengan parameter root, arr, dan top. Dalam hal ini, fungsi HuffmanCodes bertanggung jawab untuk menampilkan kode Huffman dari setiap karakter yang terdapat pada pohon Huffman.
```c
void printHCodes(struct MinHNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->item)!=NULL) {
    printArray(arr, top, root->item);
  }
}

// Wrapper function
void HuffmanCodes(char item[], int freq[], int size) {
  struct MinHNode *root = buildHuffmanTree(item, freq, size);

  int arr[MAX_TREE_HT], top = 0;

  printHCodes(root, arr, top);
}

```
Ketuju, Fungsi pertama adalah printArray(), yang digunakan untuk mencetak isi dari sebuah array ke dalam sebuah file. Fungsi ini kemudian akan mencetak karakter c ke dalam file, diikuti dengan isi dari array arr[] sebagai bilangan biner, dan ditambahkan karakter newline (\n) sebagai penanda akhir baris. 

Fungsi kedua adalah count_line(), yang digunakan untuk menghitung jumlah baris dalam sebuah file. Fungsi ini memiliki satu parameter yaitu filename[] yang merupakan nama dari file yang akan dihitung jumlah barisnya. Fungsi ini akan membuka file tersebut dengan mode "r" (read), kemudian membaca file baris per baris menggunakan fungsi fgets() dan menambahkan jumlah baris sebanyak 1 setiap kali berhasil membaca sebuah baris. Setelah selesai membaca, file akan ditutup dan fungsi akan mengembalikan jumlah baris yang berhasil dihitung.
```c
// Print the array
void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    // Check if file opened successfully
    if (fp == NULL) {
        printf("Error opening file\n");
        return 1;
    }

  int i;

  fprintf(fp, "%c", c);
  for (i = 0; i < n; ++i){
    // Append string to file
    fprintf(fp, "%d", arr[i]);
  }

  fprintf(fp, "%c", '\n');
  fclose(fp);
}

int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}
```
Kedelapan, Fungsi findLength() digunakan untuk mencari panjang dari kode Huffman yang merepresentasikan sebuah karakter tertentu pada file teks yang telah dienkripsi. Fungsi ini menerima satu parameter berupa karakter c, yang akan dicari kodenya pada file teks. Fungsi membuka file "temp.txt" yang berisi daftar karakter dan kode Huffman yang telah di-generate pada proses enkripsi menggunakan algoritma Huffman. Selanjutnya, fungsi akan membaca file "temp.txt" secara baris per baris menggunakan fgets(), dan mencari baris yang mengandung karakter c menggunakan fungsi strchr(). Jika baris yang ditemukan mengandung karakter c, maka panjang dari kode Huffman pada baris tersebut dapat dihitung dengan mengurangi panjang baris tersebut dengan 1 (karena baris tersebut juga mengandung karakter newline '\n' pada akhirnya). Fungsi mengembalikan nilai tersebut sebagai hasil.

Fungsi translate() digunakan untuk melakukan proses dekompresi pada file teks yang telah dienkripsi menggunakan algoritma Huffman. Fungsi ini menerima satu parameter berupa nama file yang akan didekompresi. Fungsi membuka tiga file, yaitu file dengan nama yang diberikan pada parameter, file "temp.txt" yang berisi daftar karakter dan kode Huffman yang telah di-generate pada proses enkripsi menggunakan algoritma Huffman, dan file "file.txt" yang akan digunakan sebagai file output hasil dekompresi. Selanjutnya, fungsi membaca file dengan nama yang diberikan pada parameter secara karakter per karakter menggunakan fgetc(). Setiap karakter yang dibaca akan dicari kodenya pada file "temp.txt" menggunakan loop while() dan fgets(). Jika kode Huffman yang sesuai ditemukan, maka kode Huffman tersebut akan di-write ke file "file.txt" menggunakan fwrite(). Fungsi mengembalikan jumlah karakter pada file hasil dekompresi sebagai hasil.
```c
int findLength(char c){
    char line[512];
    FILE *fp = fopen("temp.txt", "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
    if (strchr(line, c) != NULL) {
        //translate
        return strlen(line) - 1;
    }
}


    fclose(fp);
    return -1;
}

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

    if (fp == NULL || fs == NULL || fc == NULL) {
        printf("Error opening file.\n");
        return;
    }

    char line[512];
    char c;
    int count = 0;
    while ((c = fgetc(fc)) != EOF) {
        fseek(fs, 0, SEEK_SET);
        c = toupper(c);
        while (fgets(line, 512, fs) != NULL) {
            if (line[0] == c) {
                char *sub = malloc(strlen(line));
                strcpy(sub, line + 1);
                count += strlen(sub)-1;
                sub[strlen(sub) - 1] = '\0';
                fwrite(sub, strlen(sub), 1, fp);
                free(sub);
                break;
            }
        }
    }


    fclose(fp);
    fclose(fs);
    fclose(fc);
    return count;
}

```
Kesembilan, implementasi dari pembuatan dua pipes (fd1 dan fd2) menggunakan fungsi pipe(), dan fork sebuah child process menggunakan fungsi fork(). Fungsi pipe() digunakan untuk membuat dua buah file descriptor untuk membaca dan menulis pada pipes.

Kemudian, pada bagian pid = fork();, terjadi pembuatan child process yang dilakukan dengan memanggil fungsi fork(). Fungsi ini akan menghasilkan dua proses yaitu parent dan child. Parent akan mendapatkan nilai pid dari child process, sementara child process akan mendapatkan nilai pid 0.
```c
int main() {
    int fd1[2], fd2[2];
    pid_t pid;

    // Buat dua pipes
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    // Buat child process
    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
```
Kesepuluh, Pada bagian ini, **child process akan membaca data dari pipe dengan menggunakan fungsi read()**. Data yang akan dibaca adalah array of char arr, array of int arrint, dan char sent, yang masing-masing merepresentasikan karakter-karakter yang diencode, frekuensi karakter-karakter tersebut, dan pesan yang akan diencode.
```c
    if (pid == 0) { // child process
        char *arr = malloc(sizeof(char) * 26);
        int *arrint = malloc(sizeof(int) * 26);
        char *sent = malloc(sizeof(char)*26);
        close(fd1[1]);
        read(fd1[0], arr, sizeof(char) * 26);
        read(fd1[0], arrint, sizeof(int) * 26);
        read(fd1[0], sent, 26);
        close(fd1[0]);

        strcpy(send, sent);

        close(fd1[0]);
        HuffmanCodes(arr, arrint, sizeof(char)*26);

        close(fd2[0]);
        int countLine = count_line("temp.txt");
        write(fd2[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
            if(fp==NULL){
                perror("fail to open");
                exit(1);
            }

            char line[512];
            while(fgets(line, 512, fp)!= NULL){
                write(fd2[1], &line, sizeof(line));
            }
        close(fd2[1]);
```
Kesebelas, pada **parent process** mengimplementasi **algoritma Huffman** untuk **melakukan kompresi file** dengan membangun tree Huffman berdasarkan **frekuensi kemunculan setiap karakter pada file** input. Pada bagian ini, dilakukan pembacaan file input dengan nama **file.txt** dan dilakukan penghitungan frekuensi kemunculan setiap karakter alfabet pada file tersebut. 

Setiap karakter yang terbaca pada file input akan dicek apakah karakter tersebut merupakan karakter alfabet. Jika iya, maka karakter tersebut akan diubah menjadi **huruf kapital** dan dihitung frekuensi kemunculannya pada array freq yang memiliki 26 elemen yang merepresentasikan **26 huruf alfabet dalam bahasa Inggris**. 

Selain itu, karakter yang sama hanya akan dihitung sekali dan disimpan pada array alpha. Frekuensi kemunculan setiap huruf dan alfabet akan digunakan pada **proses pembangunan tree Huffman untuk menentukan kode biner yang akan digunakan dalam proses kompresi file**.
```c
    } else { // parent process
        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL) {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0}; 
        char alpha[26] = {'\0'}; 
        int idx = 0; 
        while ((c = fgetc(fp)) != EOF) {
            if (isalpha(c)) { // check if character is alphabetic
                c = toupper(c); // convert to uppercase
                int pos = c - 'A'; // get position in frequency array
                freq[pos]++; // increment frequency of corresponding letter
                if (freq[pos] == 1) { // if first occurrence of letter
                    alpha[idx] = c; // add letter to alphabet array
                    idx++; // increment index for alphabet array
                }
            }
        }
```
Keduabelas, Membuat alokasi memori dinamis untuk dua array yaitu arr dan arrfr dengan ukuran 26. Mengecek frekuensi dari setiap huruf pada file yang dibuka sebelumnya dan menyimpannya ke dalam array arrfr. Menyimpan karakter huruf yang muncul dalam file ke dalam array send, dan menghitung berapa banyak huruf yang ada di file. Menyimpan alfabet dalam array alpha. Mengcopy alfabet dari array alpha ke array arr. Menutup file yang telah dibuka sebelumnya.
```c
        char *arr = malloc(sizeof(char) * 26);
        int *arrfr = malloc(sizeof(int) * 26);

        // Print frequency of each letter
        int j = 0;
        for (int i = 0; i < 26; i++) {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if(freq[i]!=0){
                send[j] = alpha[i];
                j++;
            }
        }

        // Print alphabet list
        for (int i = 0; i < 26; i++) {
            arr[i] = alpha[i];
        }

        // Close file
        fclose(fp);

```
Ketigabelas,  membuat pipe untuk mengirim data antara parent process dan child process. Kode ini digunakan oleh child process untuk menulis data ke dalam pipe fd1 dan membaca data dari pipe fd2. setelah pipe fd1 dibuat, child process menulis array arr, arrfr, dan send ke dalam pipe fd1 menggunakan fungsi write. Kemudian, pipe fd1 ditutup untuk menandakan bahwa child process sudah selesai menulis ke dalam pipe fd1.

Selanjutnya, child process membaca integer n dari pipe fd2 menggunakan fungsi read. Setelah itu, child process membaca n buah string coded[i] dari pipe fd2 menggunakan fungsi read dan menyimpannya ke dalam array coded.

Terakhir, child process melakukan perhitungan jumlah bit yang diperlukan untuk merepresentasikan seluruh karakter yang muncul di dalam file. Jumlah bit ini dihitung berdasarkan frekuensi kemunculan karakter dan dihitung sebagai jumlah frekuensi kemunculan karakter dikali 8 bit untuk setiap karakter yang muncul di dalam file. Jumlah total bit yang dibutuhkan kemudian disimpan dalam variabel number.
```c
        close(fd1[0]);
        write(fd1[1], arr, sizeof(char) * 26);
        write(fd1[1], arrfr, sizeof(int) * 26);
        write(fd1[1], send, sizeof(send));
        close(fd1[1]);

        close(fd2[1]);
        int n;
        read(fd2[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++) {
            read(fd2[0], coded[i], sizeof(char) * 512);
        }

        close(fd2[0]);
        int number = 0;
        for(int i = 0; i<26; i++){
            int calc;
            if(freq[i]!=0){
                calc = freq[i]*8;
                number += calc;
            }
        }

```
Keempatbelas, mencetak jumlah bit dalam file sebelum dan setelah proses kompresi menggunakan metode Huffman. Kemudian, kode memanggil fungsi translate yang digunakan untuk menerjemahkan file yang telah dikompresi dan mencetak jumlah bit dalam file yang diterjemahkan. Akhirnya, kode menggunakan perintah sistem untuk menghapus file sementara "temp.txt" yang digunakan selama proses kompresi. Setelah itu, kode mengembalikan nilai 0 yang menunjukkan bahwa program selesai dengan sukses.
```c
       printf("%d BIT SEBELUM HUFMAN\n", number);

        number = translate("translated.txt");;
        printf("%d AFTER HUFMAN\n", number);

        system("rm temp.txt");
    }
    return 0;
}
```
#####  Output 
![image](/uploads/7c2cc02d7cbbea795c8dfd506f05b43d/image.png)


### Soal 2
**a.** Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

**b.**  Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

**c.**  Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
- array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],
maka:
1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

**d.**  Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 

#### Penyelesaian
**a.** Membuat program C dengan nama **kalian.c**

Pertama , membuat baris yang mendefinisikan ukuran-ukuran matriks yang akan digunakan dalam program ini, yaitu ROW1 x COL1 untuk matriks pertama, dan ROW2 x COL2 untuk matriks kedua.
```c
#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
```
Kedua, membuat fungsi main() dan mendeklarasikan tiga buah matriks yang akan digunakan dalam program ini, yaitu matriks1, matriks2, dan hasil. Selain itu, ada juga deklarasi beberapa variabel i, j, k, dan sum yang akan digunakan dalam perulangan-perulangan nanti. 

Kemudian, srand(time(NULL)) akan menginisialisasi seed untuk fungsi rand() dengan menggunakan waktu saat ini, sehingga setiap kali program dijalankan, nilai acak yang dihasilkan oleh fungsi rand() akan berbeda-beda.
```c
int main()
{
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], hasil[ROW1][COL2];
    int i, j, k, sum;

    srand(time(NULL)); // inisialisasi random seed
```
Ketiga, melakukan inisialisasi matriks1 dan matriks2 dengan nilai acak yang dihasilkan oleh fungsi rand(). Untuk matriks1, setiap elemen diisi dengan nilai acak antara 1-5, sedangkan untuk matriks2, setiap elemen diisi dengan nilai acak antara 1-4.
```c
    // inisialisasi matriks pertama dengan angka random 1-5
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            matriks1[i][j] = rand() % 5 + 1;
        }
    }

    // inisialisasi matriks kedua dengan angka random 1-4
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            matriks2[i][j] = rand() % 4 + 1;
        }
    }
```
Keempat, dilakukan operasi perkalian matriks antara matriks1 dengan matriks2. Setiap kali melakukan operasi perkalian, hasil dari perkalian tersebut ditambahkan ke variabel sum. 

Setelah selesai melakukan iterasi k, nilai dari variabel sum akan disimpan ke dalam elemen hasil[i][j], sebagai hasil dari perkalian antara baris ke-i pada matriks1 dengan kolom ke-j pada matriks2.
```c
    // operasi perkalian matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            sum = 0;
            for (k = 0; k < ROW2; k++) {
                sum += matriks1[i][k] * matriks2[k][j];
            }
            hasil[i][j] = sum;
        }
    }
```
Kelima, melakukan proses pembuatan shared memory, meng-copy data hasil perkalian matriks dari program ke dalam shared memory. Selanjytnya melakukan shared memory dengan menggunakan fungsi shmget(). 

Fungsi ini akan mengembalikan nilai shmid yang merupakan identifier dari shared memory yang dibuat. Selanjutnya, shared memory tersebut di-attach ke variabel hasilShared dengan menggunakan fungsi shmat() dengan parameter shmid, NULL, dan flag 0. Fungsi ini akan mengembalikan alamat dari shared memory yang berhasil di-attach. 

Setelah itu, data hasil perkalian matriks yang tersimpan pada variabel hasil di-copy ke dalam shared memory yang sudah di-attach sebelumnya dengan menggunakan loop for. Kemudian, shared memory tersebut di-detach dengan menggunakan fungsi shmdt() dengan parameter hasilShared.
```c
// membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW1][COL2]), IPC_CREAT | 0666);

    // meng-attach shared memory ke variabel hasilShared
    int (*hasilShared)[COL2] = shmat(shmid, NULL, 0);

    // meng-copy hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            hasilShared[i][j] = hasil[i][j];
        }
    }

    // detaching shared memory
    shmdt(hasilShared);
```
Keenam, matriks hasil ditampilkan ke layar dengan menggunakan loop for dan perintah printf().
```c
// tampilkan matriks hasil
    printf("Hasil perkalian matriks dari program kalian.c\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
    return 0;
}
```
**b.**  Membuat program C kedua dengan nama **cinta.c** denggan thread dan multithreading.

Pertama, membuat baris yang mendefinisikan ukuran-ukuran matriks yang akan digunakan dalam program ini, yaitu ROW 4 x COL 5 dan struktur data yang digunakan sebagai argumen pada thread.
```c
#define ROW 4
#define COL 5

struct thread_arg {
    int row;
    int col;
    int value;
    unsigned long long *factorial_ptr;
};
```
Kedua, Fungsi untuk menghitung nilai faktorial pada setiap elemen matriks menggunakan thread. Fungsi ini mengambil argumen dari struktur data "thread_arg".
```c
void *factorial_thread(void *arg) {
    struct thread_arg *t_arg = (struct thread_arg *) arg;
    int num = t_arg->value;
    unsigned long long factorial = 1;
    if (num == 0) {
        factorial = 1;
    } else {
        for (int k = 1; k <= num; k++) {
            factorial *= k;
        }
    }
    *(t_arg->factorial_ptr) = factorial;
    pthread_exit(NULL);
}
```
Ketiga, Membuat shared memory dengan menggunakan fungsi "shmget()" dengan kunci "key" dan ukuran sebesar "sizeof(int[ROW][COL])". Variabel "result" akan menautkan shared memory dengan menggunakan fungsi "shmat()". Kemudian, program akan mencetak hasil perkalian matriks yang telah disimpan dalam shared memory.
```c
int main() {
    int shmid;
    key_t key = 1234;
    int (*result)[COL];

    // Membuat shared memory
    shmid = shmget(key, sizeof(int[ROW][COL]), IPC_CREAT | 0666);

    // Menautkan shared memory ke dalam variabel result
    result = shmat(shmid, NULL, 0);

    // Mengambil hasil perkalian matriks dari shared memory yang telah diisi oleh program kalian.c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
Keempat, menghitung faktorial untuk setiap angka pada matriks dengan menggunakan thread Membuat array dua dimensi bernama result dengan ukuran ROW dan COL.Mengisi result dengan bilangan bulat acak antara 1 hingga 10.Membuat array bernama factorial_array dengan ukuran ROW * COL, yang akan menyimpan hasil faktorial dari setiap angka pada matriks. 

Membuat struktur thread_arg yang berisi informasi yang akan digunakan oleh thread.Membuat thread sejumlah ROW * COL. Setiap thread akan menerima argumen berupa thread_arg yang berisi informasi tentang baris, kolom, dan nilai dari matriks yang akan dihitung faktorialnya, serta alamat array factorial_array yang akan menyimpan hasil faktorial.

Pada fungsi factorial_thread, setiap thread akan menghitung faktorial dari nilai yang diberikan, dan hasilnya akan disimpan pada lokasi yang sesuai pada array factorial_array.Program menunggu seluruh thread selesai dengan menggunakan pthread_join. Program mencetak hasil faktorial dari setiap angka pada matriks.
```c
 // Menghitung faktorial untuk setiap angka pada matriks dengan menggunakan thread
    printf("Hasil faktorial untuk setiap angka pada matriks:\n");
    clock_t start_time = clock();
    unsigned long long factorial_array[ROW * COL];
    int factorial_count = 0;
    pthread_t threads[ROW * COL];
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            struct thread_arg *t_arg = (struct thread_arg *) malloc(sizeof(struct thread_arg));
            t_arg->row = i;
            t_arg->col = j;
            t_arg->value = result[i][j];
            t_arg->factorial_ptr = &factorial_array[factorial_count];
            pthread_create(&threads[factorial_count], NULL, factorial_thread, (void *) t_arg);
            factorial_count++;
        }
    }
```
**c.** Mencetak hasil faktorial dengan array satu dimensi didalam pthread_join
```c
    // Join thread
    for (int i = 0; i < factorial_count; i++) {
        pthread_join(threads[i], NULL);
        printf("%llu ", factorial_array[i]);
    }
```
**d.** Membuat program C ketiga dengan nama **sisop.c.** tanpa thread dan multithreading.

Pertama , mendefinisikan beberapa konstanta yaitu ROW dan COL yang masing-masing merepresentasikan jumlah baris dan kolom pada matriks yang akan diolah. Selanjutnya program mendefinisikan variabel shmid untuk menampung id shared memory, variabel key sebagai kunci shared memory, dan variabel result sebagai pointer ke array 2 dimensi.
```c
#define ROW 4
#define COL 5

int main() {
    int shmid;
    key_t key = 1234;
    int (*result)[COL];
```
Kedua, membuat membuat shared memory dengan ukuran sebesar matriks yang akan diolah menggunakan fungsi shmget(). Fungsi tersebut menerima parameter berupa key, ukuran shared memory, dan hak akses untuk shared memory. Setelah shared memory dibuat, program menautkannya ke dalam variabel result menggunakan fungsi shmat(). 

Fungsi tersebut menerima parameter berupa id shared memory, alamat buffer shared memory, dan flag yang menentukan bagaimana buffer akan diattach ke process.Setelah itu mencetak hasil perkalian matriks yang telah dimasukkan oleh program lain ke dalam shared memory.
```c
    // Membuat shared memory
    shmid = shmget(key, sizeof(int[ROW][COL]), IPC_CREAT | 0666);

    // Menautkan shared memory ke dalam variabel result
    result = shmat(shmid, NULL, 0);

    // Mengambil hasil perkalian matriks dari shared memory yang telah diisi oleh program kalian.c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
```
Ketiga, perhitungan faktorial untuk setiap angka pada matriks yang telah diambil dari shared memory sebelumnya. Melakukan nested loop sebanyak ROW dan COL untuk mengakses setiap elemen matriks result. Setiap elemen result[i][j] akan dihitung faktorialnya dengan menggunakan loop for dengan variabel k.Hasil faktorial dari setiap elemen matriks akan disimpan di dalam array factorials. 
```c
    // Menghitung faktorial untuk setiap angka pada matriks tanpa menggunakan thread
    printf("Hasil faktorial untuk setiap angka pada matriks:\n");
    clock_t start_time = clock();
    unsigned long long factorials[ROW * COL];
    int index = 0;
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            int num = result[i][j];
            unsigned long long factorial = 1;
            if (num == 0) {
                factorial = 1;
            } else {
                for (int k = 1; k <= num; k++) {
                    factorial *= k;
                }
            }
            factorials[index++] = factorial;
        }
    }
    for (int i = 0; i < ROW * COL; i++) {
        printf("%llu ", factorials[i]);
    }
    printf("\n");
```
Keempat, Untuk membandingkan program yang menggunakan thread dan tanpa thread maka dama program **cinta.c** dan program **sisop.c** kami menambahkan perhitungan waktu, dan CPU yang digunakan saat program menghitung faktorialnya dengan kode berikut :
```c
//Hitung Waktu
    clock_t end_time = clock();
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("\nElapsed time: %f seconds\n", elapsed_time);
    printf("\n");

    //Hitung CPU usage
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    double user_time = (double) usage.ru_utime.tv_sec + ((double) usage.ru_utime.tv_usec / 1000000);
    double system_time = (double) usage.ru_stime.tv_sec + ((double) usage.ru_stime.tv_usec / 1000000);
    double total_time = user_time + system_time;

    double num_cpus = sysconf(_SC_NPROCESSORS_ONLN);
    double cpu_usage_percent = (total_time / num_cpus) * 100;

    printf("CPU usage: %.2f%%\n", cpu_usage_percent);


    // Hitung memory usage statistics
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    long total_memory = pages * page_size;
    long rss_memory = 0;
    FILE *fp = fopen("/home/bilaaripa/Praktikum3/soal3/sisop.c", "r");
    if (fp != NULL) {
        fscanf(fp, "%*s%ld", &rss_memory);
        fclose(fp);
    }
    printf("Total memory: %ld bytes\n", total_memory);
    printf("RSS memory: %ld bytes\n", rss_memory * page_size);

    // Melepaskan shared memory
    if (shmdt(result) == -1) {
        perror("shmdt");
        exit(1);
    }
    if (shmctl(shmid, IPC_RMID, NULL) == -1) {
        perror("shmctl");
        exit(1);
    }
    return 0;
}
```
#### Output
![Screenshot__2390_](/uploads/bde43612d5b05f29e31034ff77c68a36/Screenshot__2390_.png)

![Screenshot__2391_](/uploads/caefa311bf99b1ef69a67ed9043e7562/Screenshot__2391_.png)

### Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

**a.** Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

**b.** User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.

**c.** Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

**d.** User juga dapat mengirimkan perintah PLAY <SONG> 

**e.** User juga dapat menambahkan lagu ke dalam playlist dengan perintah ADD

**f.** Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist. Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

**g.** Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

#### Penyelesaian
**a.** Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

Untuk soal bagian ini, digunakan message queue, dimana user.c bertindak sebagai writer, dan stream.c bertindak sebagai reader.

berikut merupakan bagaian write pada user.c
```c
key_t key;
    int msgid;
    char input[MAX_TEXT];

    key = ftok("progfile", KEY_VALUE);

    msgid = msgget(key, 0666 | IPC_CREAT);
    
    while (1) {
        message_buffer message;
        message.message_type = 1;
        printf("Enter Data (0 to exit): ");
        fgets(input, MAX_TEXT, stdin);
        if (strcmp(input, "0\n") == 0) break;

        char *first_word = malloc(sizeof(input));
        first_word = strcpy(first_word,input);
        first_word = strtok(first_word, " ");
        if(strcmp(first_word, "PLAY")!=0 && strcmp(first_word, "ADD")!=0){
            toLowercase(input);
        }
        
        input[strlen(input)-1] = '\0';
        // copy the message text to message buffer
        strncpy(message.message_text, input, MAX_TEXT);

        // send message to message queue
        send_message(msgid, message, 1);
    }
```

Kemudian, akan dibaca oleh stream.c
```c
key_t key;
    int msgid;

    key = ftok("progfile", KEY_VALUE);

    msgid = msgget(key, 0666 | IPC_CREAT);

    message_buffer message;
    int count = 0;
    while (msgrcv(msgid, &message, sizeof(message), 1, IPC_NOWAIT) != -1) {
        ...
    }
```


**b.** User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.

Untuk menyelesaikan ini, karena ada 3 jenis metode dekripsi/decode, maka akan dibuat tiga fungsi dan satu fungsi driver:
```c
message_buffer decrypt(char *cmd, char song[]);
message_buffer decrypt_rot13(char str[], int length);
message_buffer decrypt_base64(char str[], int length) ;
message_buffer decrypt_hex(char str[], int length)

message_buffer decrypt_rot13(char str[], int length){
    int i;
    for (i = 0; i<length ; i++) {
        char c = str[i];
        if (isalpha(c)) {
            if (isupper(c)) {
                str[i] = ((c - 'A' + 13) % 26) + 'A';
            } else {
                str[i] = ((c - 'a' + 13) % 26) + 'a';
            }
        }
    }

    return build_message(str, 2);
}

message_buffer decrypt_base64(char str[], int length){
    char command[1024];
    sprintf(command, "echo %s | base64 -d", str);

    FILE *p = popen(command, "r");
    if(p==NULL){
        printf("Error opening pipe with given command");
        return build_message("error openinng pipe", -1);
    }

    char output[length+1];
    fgets(output, sizeof(output), p);
    fclose(p);

    output[strcspn(output, "\n")] = '\0';
    return build_message(output, 2);
}

message_buffer decrypt_hex(char str[], int length){
    char command[1024];
    sprintf(command, "echo %s | hex -d", str);

    FILE *p = popen(command, "r");
    if(p==NULL){
        printf("Error opening pipe with given command");
        return build_message("error opening pipe", -1);
    }

    char output[length+1];
    fgets(output, sizeof(output), p);
    fclose(p);

    output[strcspn(output, "\n")] = '\0';
    return build_message(output, 2);
}

message_buffer decrypt(char *cmd, char song[]){
    if(strcmp(cmd, "rot13")==0){
        return decrypt_rot13(song, strlen(song));
    }
    else if(strcmp(cmd, "base64") == 0){
        return decrypt_base64(song, strlen(song));
    }
    else if(strcmp(cmd, "hex") == 0){
        return decrypt_hex(song, strlen(song));
    }
    
    return build_message("UNKNOWN COMMAND", 2);
}
```

berikut merupakan pemanggilan fungsi decrypt()
```c
char *json_str = getJsonString(FILENAME);
processJson(json_str);

void processJson(char *json_str){
    struct json_object *json_array = json_tokener_parse(json_str);
    if (!json_array || json_object_get_type(json_array) != json_type_array) {
        fprintf(stderr, "Error: Invalid JSON array\n");
        free(json_str);
        return;
    }

    // Iterate over the JSON objects in the array and print their "method" and "song" values
    size_t array_len = json_object_array_length(json_array);
    for (size_t i = 0; i < array_len; i++) {
        json_object *json_obj = json_object_array_get_idx(json_array, i);
        json_object *method_obj, *song_obj;

        if (!json_object_object_get_ex(json_obj, "method", &method_obj) ||
            !json_object_object_get_ex(json_obj, "song", &song_obj)) {
            fprintf(stderr, "Error: Missing attribute in JSON object at index %zu\n", i);
            continue;
        }

        char const *method = json_object_get_string(method_obj);
        char const *song = json_object_get_string(song_obj);

        //process it
        message_buffer msg = decrypt(method, song);
        
        FILE *fp = fopen(PLFILE, "a");
        if (fp == NULL) {
            fprintf(stderr, "Error opening file\n");
            exit(EXIT_FAILURE);
        }
        fprintf(fp, "%s\n", msg.message_text);
        fclose(fp);
    }

    json_object_put(json_array);
    free(json_str);
}
```

Perlu dicatat bahwa implementasi untuk memproses data .json dilakukan dengan menggunakan library external, json-c.

Dimana untuk fungsi hex dan base64 menggunakan system(). Berikut merupakan output dari fungsi di atas : : 
![image](/uploads/de0637e084fddd1c94ff1d38ab18624d/image.png) 

**c.** Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

```c
void list_pL(){
    char command[512];
    sprintf(command, "sort -o %s %s", PLFILE, PLFILE);
    system(command);
}
```

#### Revisi soal 3.c

Dapat dilihat pada fungsi di atas, list_pl() tidak akan menampilkan isi dari file ke terminal. oleh karena itu, perlu dimodifikasi menjadi
```c
void list_pL(){
    char command[512];
    sprintf(command, "sort -o %s %s", PLFILE, PLFILE);
    system(command);

    char cmd[512];
    sprintf(cmd, "cat %s", PLFILE);
    system(cmd);
}
```

Berikut merupakan output :
![image](/uploads/2b7a040a4b98f666529bfa239cc48b0b/image.png)

**d.** User juga dapat mengirimkan perintah PLAY <SONG>

Dengan ketentuan sesuai soal, jika <song> yang diinputkan ternyata menghasilkan tepat satu lagu, maka akan ditampilkan lagu tersebut.
Namun jika <song> menghasilkan lebih dari satu, maka akan ditampilkan semua, dan jika tidak ada, maka akan ditampilkan "THERE IS NO SONG CONTAINING <song>"

```c
message_buffer play_song(char *song, int length){
    char command[512];
    sprintf(command, "grep \"%s\" %s", song, PLFILE);

    FILE *p = popen(command, "r");
    if(p == NULL){
        printf("Error opening pipe with given command");
        return build_message("error openinng pipe", -1);
    }

    char *output = NULL;
    size_t output_size = 0;

    char buffer[1024];
    int count = 1; // add a count variable to keep track of song numbers
    while(fgets(buffer, sizeof(buffer), p)){
        char numbered_output[1024];
        sprintf(numbered_output, "%d. %s", count, buffer); // add song number to the output
        size_t buffer_len = strlen(numbered_output);
        output = realloc(output, output_size + buffer_len + 1);
        if(output == NULL){
            fclose(p);
            return build_message("out of memory", -1);
        }
        memcpy(output + output_size, numbered_output, buffer_len);
        output_size += buffer_len;
        count++; // increment the song count
    }

    fclose(p);

    if(output_size > 0 && output[output_size-1] == '\n'){
        output[output_size-1] = '\0';
        output_size--;
    }

    if(count-1 == 1){
        char *formatted_output = malloc(strlen(output) + strlen(song) + 32); // allocate new buffer
        sprintf(formatted_output, "USER %d IS PLAYING \"%s\"", current_userid, output + 2); // format new string, skipping song number
        return build_message(formatted_output, 2);

    } else if(count - 1 == 0){
        char *response = malloc(strlen(song) + 32); // allocate new buffer
        sprintf(response, "THERE IS NO SONG CONTAINING \"%s\"", song);
        return build_message(response, 2);
    }

    char header[96+strlen(song)];
    sprintf(header, "THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", count-1, song);

    printf("%s", header);
    char cmd[512];
    sprintf(cmd, "grep \"%s\" \"%s\" >> %s", song, PLFILE, "temp.txt");
    system(cmd);
    
    print_numbered_lines("temp.txt");
    return build_message("Default", 13);
}
```

Berikut merupakan output dari pemanggilan PLAY :
![image](/uploads/5b589d8e596fd83b98ef5bd330d00224/image.png)

Hiraukan "Allowed ID : ...", akan dijelaskan pada poin - poin berikutnya.

**e.** User dapat menambahkan lagu ke playlist.txt menggunakan perintah ADD

Dengan ketentuan jika lagu yang ingin ditambahkan sudah ada pada playlist, maka
akan ditampilkan "SONG ALREADY ON PLAYLIST". Jika lagu belum ada, maka lagu tersebut akan
dimasukkan ke dalam playlist, dan akan ditampilkan "USER <ID> ADD <song>"

```c
message_buffer add_song(char *song, int length){
    /*
    1.find out if the song is already exist or not
    2.add the song
    */    
    
    char cmd[512];
    sprintf(cmd, "grep \"%s\" %s >> %s", song, PLFILE, "temp.txt");
    system(cmd);

    if(countLine("temp.txt")!=0){
        delete_file("temp.txt");
        return build_message("SONG ALREADY ON PLAYLIST", 2);
    }
    delete_file("temp.txt");

    char cmd2[512];
    sprintf(cmd2, "echo %s >> %s", song, PLFILE);
    system(cmd2);

    char respon[512];
    sprintf(respon, "USER %d ADD %s", current_userid, song);
    return build_message(respon, 2);
}
```

Berikut merupakan output dari pemanggilan fungsi tersebut:
![image](/uploads/8f9637e3e28cb03074a85e0b71449cbf/image.png)

**f.** Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist. Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

Bisa dilihat disini, semaphore dengan limit 2 user hanya digunakan ketika seorang user ingin mengakses playlist. Dari perintah - perintah yang diminta, ada dua 
perintah yang mengakses playlist secara langsung. Yakni ADD dan PLAY. oleh karena itu, akan dilakukan pengecekan user dan semaphore value ketika perintah merupakan
salah satu dari dua perintah tersebut.

Maka, akan dibuat dua array, satu array akan menyimpan id-id yang diperbolehkan dan satunya akan menyimpan array yang di blokir.

```c
uid_t blocked_users[MAX_BLOCKED_USERS] = {-1};
uid_t allowed_users[MAX_ALLOWRD_USERS] = {-1};

bool isBlocked(uid_t id){
    bool is_blocked = false;
        for (int i = 0; i < MAX_BLOCKED_USERS; i++) {
            if (blocked_users[i] == id) {
                is_blocked = true;
                break;
            }
       }
    
    return is_blocked;
}

bool isAllowd(uid_t id){
    bool det = false;
        for (int i = 0; i < MAX_ALLOWRD_USERS; i++) {
            if (allowed_users[i] == id) {
                det = true;
                break;
            }
       }
    
    return det;
}

void addAllowrd(uid_t id){
        for (int i = 0; i < MAX_ALLOWRD_USERS; i++) {
            if (allowed_users[i] == -1) {
                allowed_users[i] = id;
                break;
            }
       }
    
    return;
}

void addBlocked(uid_t id){
        for (int i = 0; i < MAX_BLOCKED_USERS; i++) {
            if (blocked_users[i] == -1) {
                blocked_users[i] = id;
                break;
            }
       }
    
    return;
}
```

Kemudian, untuk mengaplikasikan semaphore pada PLAY dan ADD:
```c
else if(strcmp(cmd, "PLAY")==0){
            int bfore = -1;
            if(prev_userid != current_userid){
                sem_getvalue(&sem, &bfore);
                sem_trywait(&sem);
                sem_getvalue(&sem, &ret);
            }
            

            if (bfore == 0 && !isAllowd(current_userid)) { // full
                isFull = true;
                addBlocked(current_userid);
                printf("OVERLOADED ID %d :", current_userid);
                respond = build_message("OVERLOADED", 2);
            
            } else {
                cmd = strtok(NULL, "\"");
                if(cmd != NULL){
                    addAllowrd(current_userid);
                    printf("ALLOWED ID %d :", current_userid);
                    respond = play_song(cmd, strlen(cmd));
                }else{
                    respond = build_message("WRONG FORMAT", 2);
                }
            }
        }

        else if(strcmp(cmd, "ADD")==0){
            int bfore = -1;
            if(prev_userid != current_userid){
                sem_getvalue(&sem, &bfore);
                sem_trywait(&sem);
                sem_getvalue(&sem, &ret);
            }
            

            if (bfore == 0 && !isAllowd(current_userid)) { // full
                isFull = true;
                addBlocked(current_userid);
                printf("OVERLOADED ID %d :", current_userid);
                respond = build_message("OVERLOADED", 2);
            
            } else {
                cmd = strtok(NULL, "\"");
                if(cmd != NULL){
                    addAllowrd(current_userid);
                    printf("ALLOWED ID %d :", current_userid);
                    respond = add_song(cmd, strlen(cmd));
                }else{
                    respond = build_message("WRONG FORMAT", 2);
                }
            }
        }
```

Perlu dicatat bahwa pada awal fungsi main di stream.c, sudah di buat semaphore dengan value 2.
```c
sem_init(&sem, 0, 2);
```

Berikut merupakan contoh output :
![image](/uploads/646c350c85d9009640e64d07c52b7e78/image.png)

**g.** Jika perintah tidak diketahui, tampilkan UNKNOWN COMMAND

```c
 else {
            respond = build_message("UNKNOWN COMMAND", 2);
        }
```

Contoh Output:
![image](/uploads/7858fb3bbee50132062fef68aaf04294/image.png)

### Soal 4

Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama `hehe.zip`. Di dalam file .zip tersebut, terdapat sebuah folder bernama `files` dan file `.txt` bernama `extensions.txt` dan `max.txt`. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

**a.** Download dan unzip file tersebut dalam kode c bernama unzip.c.

**b.** Selanjutnya, buatlah program `categorize.c` untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file `extensions.txt`. Buatlah folder `categorized` dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam `.txt` files tersebut akan dimasukkan ke folder `other`.
Pada file `max.txt`, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder `other`. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

**c.** Output-kan pada terminal banyaknya file tiap extension terurut ***ascending*** dengan semua lowercase, beserta `other` juga dengan format sebagai berikut.

- **extension_a : banyak_file**
- **extension_b : banyak_file**
- **extension_c : banyak_file**
- **other : banyak_file**

**d.** Setiap pengaksesan folder, sub-folder, dan semua folder pada program `categorize.c` wajib menggunakan ***multithreading***. Jika tidak menggunakan akan ada pengurangan nilai.

**e.** Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program `categorize.c` buatlah log dengan format sebagai berikut.
- **DD-MM-YYYY HH:MM:SS ACCESSED [folder path]**
- **DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]**
- **DD-MM-YYYY HH:MM:SS MADE [folder name]**

Catatan:
- Path dimulai dari folder `files` atau `categorized`
- Simpan di dalam `log.txt`
- ACCESSED merupakan folder files beserta dalamnya
- Urutan log tidak harus sama

**f.** Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama `logchecker.c` untuk mengekstrak informasi dari `log.txt` dengan ketentuan sebagai berikut.
- Untuk menghitung banyaknya ACCESSED yang dilakukan.
- Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ***ascending***.
- Untuk menghitung banyaknya total file tiap extension, terurut secara ***ascending***.

#### Penyelesaian

**a.** Membuat program `unzip.c` untuk melakukan download dan unzip. Proses ini dapat melalui fork child dan parent process. Pada `child process` akan melakukan download, sedangkan `parent process` akan melakukan unzip. Berikut kodenya,

```c
pid_t child_id;
int status;

child_id = fork();

if (child_id < 0) 
{
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
}

if (child_id == 0) 
{
    // this is child
    //printf("masuk download\n");
    char link[100];

    sprintf(link, "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp");

    char *argv[] = {"wget", "-qO", "/home/arfikra/Documents/prak_sisop/modul3/no4rev/hehe.zip", link, NULL};

    execv("/usr/bin/wget", argv);
} 
```
Berikut unzip process yang terjadi pada `parent process`
```c
else 
{
   // this is parent
   while ((wait(&status)) > 0);

   //printf("masuk unzip\n");
   char *args[] = {"unzip", "-q" ,"/home/arfikra/Documents/prak_sisop/modul3/no4rev/hehe.zip", "-d", "/home/arfikra/Documents/prak_sisop/modul3/no4rev",NULL};

   execv("/usr/bin/unzip", args);
}
```

**b.** Proses kategori file ini dilakukan melalui program `categorize.c`. Sebelum melakukan proses kategori, terlebih dahulu dibuat sebuah folder bernama `categorized`. Di dalam folder ini, nantinya akan berisi subfolder dengan nama ekstensi tertentu. Sebelum itu, kita perlu mengakses ekstensi-ekstensi yang telah ditentukan dan jumlah maksimal file pada suatu folder. Ekstensi dan jumlah maksimal ini diperoleh dari hasil eksekusi program `unzip.c` sebelumnya, tepatnya pada dua file `.txt` yaitu `extension.txt` dan `max.txt`. Berikut kode yang melakukan akses pada file `extension.txt` dan `max.txt`,

```c
FILE * fileExtensions = fopen("extensions.txt", "r");

char buffer[100];

while (fgets(buffer, 100, fileExtensions) != NULL) 
{
    int len = strcspn(buffer, "\r\n"); // find position of newline or carriage return character
    buffer[len] = '\0';
    strcpy(extensions[totalExt], buffer);
    totalExt++;
}
fclose(fileExtensions);
```
Pengaksesan pada file `extension.txt` ini dibantu oleh array `buffer` yang lalu dimasukkan ke array `extensions`.

```c
FILE * fileMax = fopen("max.txt", "r");

fgets(buffer, 100, fileMax);
maxFiles = atoi(buffer);

fclose(fileMax);
```
Pengaksesan pada file `max.txt` ini akan mengambil isi dari file tersebut dan merubahnya menjadi data bertipe `int` dengan bantuan fungsi `atoi()`.

Berikutnya, dilakukan pembuatan folder `categorized`
```c
void createCategorized() 
{
    system("mkdir categorized");
    madeLog("categorized");

    system("cd categorized && mkdir other");
    madeLog("other");

    for (int i = 0; i < totalExt - 1; i++) 
    {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        madeLog(extensions[i]);
    }
}
```

Setelah itu, dilakukan proses kategori dengan menerapkan ***multithreading***. Berikut kodenya,
```c
pthread_t tid;
pthread_create( & tid, NULL, categorized_thread, "./files");
pthread_join(tid, NULL);
```
fungsi yang dipanggil pada pembuatan `thread` ini yaitu `categorized_thread()` dengan argumen adalah direktori saat ini (*current working directory*). Fungsi yang digunakan untuk melakukan proses kategori yaitu `categorized_thread()` dengan fungsi pelengkapnya yaitu `categorized()`. Fungsi `categorized()` ini mengambil argumen berupa jalan direktori atau `path` bertipe pointer `char`. Berikut kodenya,

```c
void * categorized_thread(void * arg) 
{
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}
```
```c
void categorized(const char * path) {
    DIR * directory = opendir(path);

    accessLog(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;

    while ((entry = readdir(directory)) != NULL) {

        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) 
        {
            // printf("Directory: %s\n", fullPath);
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } 
        else 
        {
            char * ext = strrchr(entry -> d_name, '.');
            if (ext) 
            {
                ext = ext + 1;
                toLowercase(ext);
            } else ext = "";
            // printf("%s ", fullPath);

            char * newFullPath = addingQuotes(fullPath);

            if (isFound(ext)) 
            {
                int extIndex = getExtIndex(ext);
                extCount[extIndex]++;
                // printf("%s %d\n", ext, extIndex);

                if (extFol[extIndex] == 1) 
                {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    int n = countFiles(dest);

                    if (n < maxFiles - 1) 
                    {                
                    	// Copying process
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);
                        // printf("%s\n", command);
                        system(command);
                    
                        moveLog(ext, fullPath, ext);
                    } 
                    else 
                    {
                        extFol[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

			// give the order of the folder creation (for the duplication)
                        char num[10];
                        sprintf(num, "%d", extFol[extIndex]);
                        strcat(cmd_mkdir, num);

                        system(cmd_mkdir);
                        // printf("%s\n", cmd_mkdir);
                

                        // Making folder name for log insertion purpose
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        madeLog(folderName);

			// Copying process
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14); // after ./categorized/
                        moveLog(ext, fullPath, temp);
                    }
                } 
                else 
                {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

		    // give the order of the folder creation (for the duplication)
                    char num[10];
                    sprintf(num, "%d", extFol[extIndex]);
                    strcat(dest, num);
                    int n = countFiles(dest);

                    if (n < maxFiles - 1) 
                    {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

			// Making folder name for log insertion purpose
                        char temp[100];
                        strcpy(temp, dest + 14);
                        moveLog(ext, fullPath, temp);
                    } 
                    else 
                    {
                        extFol[extIndex]++;
                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extFol[extIndex]);
                        strcat(cmd_mkdir, num);

                        // printf("%s\n", cmd_mkdir);
                        system(cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        madeLog(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);
                        // printf("%s\n", command);
                        system(command);

                        // Making folder name for log insertion purpose       
                        char temp[100];
                        strcpy(temp, dest + 14);
                        moveLog(ext, fullPath, temp);
                    }
                }
            } 
            else 
            {
                extCount[totalExt - 1]++;
                char command[100] = "cp ";
                strcat(command, newFullPath);
                strcat(command, " \"./categorized/other\"");

                // printf("%s\n", command);
                system(command);
                moveLog(ext, fullPath, "other");
            }
        }
    }
    closedir(directory);
}
```

Kode tersebut adalah sebuah fungsi bernama `categorized()` yang berfungsi untuk mengategorikan file-file di dalam suatu direktori. Fungsi ini mengambil satu parameter yaitu `path` yang berisi path dari direktori yang ingin dikategorikan.

Pertama-tama, fungsi ini membuka direktori yang akan dikategorikan dengan menggunakan fungsi `opendir()` dan melakukan *logging* pada file yang diakses dengan menggunakan fungsi `accessLog()`.

Selanjutnya, fungsi ini melakukan iterasi pada setiap file/direktori yang ada di dalam direktori yang dibuka dengan menggunakan `readdir()`. Pada setiap iterasi, jika ditemukan file/direktori dengan nama `"."` atau `".."`, fungsi akan melakukan `continue` ke iterasi selanjutnya.

Jika file/direktori yang ditemukan adalah direktori, fungsi akan memanggil fungsi `categorized_thread()` yang akan menjalankan fungsi `categorized()` secara rekursif pada direktori tersebut dengan menggunakan `thread`.

Jika file/direktori yang ditemukan adalah file, fungsi akan melakukan pengkategorian pada file tersebut. Pertama, fungsi akan mencari ekstensi dari file tersebut dengan menggunakan `strrchr()` dan melakukan konversi ke huruf kecil dengan menggunakan `toLowercase()`. Selanjutnya, fungsi akan memeriksa apakah ekstensi dari file tersebut ada di dalam daftar ekstensi yang ingin dikategorikan dengan menggunakan `isFound()` dan `getExtIndex()`.

Jika ekstensi tersebut ditemukan di dalam daftar ekstensi yang ingin dikategorikan, fungsi akan memindahkan file ke dalam folder yang sesuai dengan ekstensi tersebut. Fungsi akan menghitung jumlah file yang ada di dalam folder tersebut dan jika belum mencapai batas maksimum (didefinisikan oleh `maxFiles`), fungsi akan melakukan pemindahan file tersebut ke dalam folder tersebut dan melakukan *logging* dengan menggunakan `moveLog()`. Jika sudah mencapai batas maksimum, fungsi akan membuat folder baru dengan nama `"namaEkstensi"+nomorUrut` dan memindahkan file tersebut ke dalam folder baru tersebut.

Jika ekstensi tidak ditemukan di dalam daftar ekstensi yang ingin dikategorikan, fungsi akan memindahkan file ke dalam folder `"other"` dan melakukan *logging* dengan menggunakan `moveLog()`.

Fungsi akan terus melakukan iterasi pada setiap file/direktori yang ada di dalam direktori yang dibuka sampai sudah tidak ada lagi file/direktori yang ditemukan. Setelah selesai melakukan iterasi, fungsi akan menutup direktori yang dibuka dengan menggunakan `closedir()`.

Untuk membantu keseluruhan proses pada program `categorize.c` ini, terdapat beberapa fungsi yang membantu, yaitu;
- fungsi `getCurrTime()` untuk mencatata waktu (*timestamp*).

- fungsi `sortExt()` yang menerapkan konsep `bubble sort`.
- fungsi `isFound()` untuk memeriksa apakah ekstensi termasuk yang terdaftar.
- fungsi `toLowercase()` untuk mengecilkan huruf (menjadikan huruf kecil).
- fungsi `addingQuotes()` untuk menambahkan tanda petik kepada sebuah `string path`.
- fungsi `countFiles()` untuk menghitung jumlah file.
- fungsi `getExtIndex()` untuk mendapatkan indeks dari ekstensi yang berada pada array `extensions`.

**c.** Untuk melakukan output ke terminal, berikut kodenya,
```c
for (int i = 0; i < totalExt; i++) 
{
    printf("%s: %d\n", extensions[i], extCount[i]);
}
```
array `extensions` berisi ekstensi yang sudah ditentukan (termasuk kelompok `"other"`), sedangkan `extCount` berisi jumlah file untuk setiap ekstensi.

**d.** Berikut potongan kode yang memperlihatkan eksekusi program dengan menerapkan **multithreading**,
```c
pthread_t tid;
pthread_create( & tid, NULL, categorized_thread, "./files");
pthread_join(tid, NULL);
```
```c
if (entry -> d_type == DT_DIR) 
{
    // printf("Directory: %s\n", fullPath);
    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, fullPath);
    pthread_join(tid, NULL);
} 
```
**e.** Setiap proses akses folder, pembuatan folder, dan pemindahan file antarfolder, dicatat ke dalam sebuah file bernama `log.txt`. Untuk setiap tahapan ini, terdapat tiga fungsi yang bertugas mencatat ke dalam `log.txt`. Ketiga fungsi itu adalah `accessLog()`, `madeLog()`, dan `moveLog()`. Cara kerja ketiga fungsi ini memiliki kesamaan dimana menggunakan cara konkatenasi `string` untuk membuat sebuah pesan atau perintah dengan bantuan fungsi `strcat()` yang lalu dieksekusi dengan fungsi `system()`. Berikut ketiga fungsi pencatat log tersebut;

fungsi `accessLog()`
```c
void accessLog(const char * path) 
{
    char timestamp[20];
    getCurrTime(timestamp);

    char logMess[100];
    logMess[0] = '\0';

    strcat(logMess, timestamp);
    strcat(logMess, " ACCESSED ");
    strcat(logMess, path);

    char command[200];
    command[0] = '\0';

    strcat(command, "echo \"");
    strcat(command, logMess);
    strcat(command, " \" >> log.txt");
    system(command);
}
```

fungsi `moveLog()`
```c
void moveLog(char * ext, char * src, char * dst) 
{
    char timestamp[20];
    getCurrTime(timestamp);

    char logMess[200];
    logMess[0] = '\0';

    strcat(logMess, timestamp);
    strcat(logMess, " MOVED ");
    strcat(logMess, ext);
    strcat(logMess, " file : ");
    strcat(logMess, src);
    strcat(logMess, " > ");
    strcat(logMess, dst);

    char command[100];
    command[0] = '\0';

    strcat(command, "echo \"");
    strcat(command, logMess);
    strcat(command, " \" >> log.txt");
    system(command);
}
```
fungsi `madeLog()`
```c
void madeLog(char * name) 
{
    char timestamp[20];
    getCurrTime(timestamp);

    char logMess[100];
    logMess[0] = '\0';

    strcat(logMess, timestamp);
    strcat(logMess, " MADE ");
    strcat(logMess, name);

    char command[100];
    command[0] = '\0';

    strcat(command, "echo \"");
    strcat(command, logMess);
    strcat(command, " \" >> log.txt");
    system(command);
}
```
Untuk melakukan pencatatan log ke dalam `log.txt`, diperlukan nilai waktu dijalankannya suatu perintah. Waktu ini, didapatkan melalui fungsi yang bernama `getCurrTime()`, berikut kodenya;
```c
void getCurrTime(char * timestamp) 
{
    time_t currTime;
    struct tm * timeForm;

    currTime = time(NULL);

    timeForm = localtime( & currTime);
    strftime(timestamp, 20, "%d-%m-%y %H:%M:%S", timeForm);
    // printf("%s\n", timestamp);
}
```
**f.** Untuk melakukan pemeriksaan terhadap `log.txt`, maka dapat dilakukan melalui program `logchecker.c`. Melalui program ini, akan diperoleh jumlah akses terhadap suatu folder, perhitungan file berdasarkan seluruh folder, dan perhitungan file berdasarkan ekstensi.

Sebelum melakukan proses perhitungan, file log.txt harus diakses terlebih dahulu dengan blok kode berikut;
```c
FILE * fp = fopen("log.txt", "r");

if (fp == NULL) 
{
    perror("Error opening file");
    exit(EXIT_FAILURE);
}
```
Untuk melakukan perhitungan akses, dapat dilakukan melalui eksekusi kode berikut;
```c
char buffer[200];
int AccessedCount = 0;
    while (fgets(buffer, 200, fp)) {
        if (strstr(buffer, "ACCESSED") != NULL) 
        {
            AccessedCount++;
        }
    }
printf("Jumlah 'ACCESSED': %d\n", AccessedCount);
```

Untuk *listing* folder, dapat dilakukan dengan bantuan array seperti pada kode berikut;
```c
fp = fopen("log.txt", "r");

char * folder;

while (fgets(buffer, 200, fp)) 
{
    folder = strstr(buffer, "MADE ");

    if (folder != NULL) 
    {
        folder += strlen("MADE ");
        folder[strlen(folder) - 2] = '\0';
        strcpy(folders[folderCount], folder);
        folderCount++;
    }
}
```

Kode di atas membuka file `"log.txt"` dalam mode `"r"` (read) dengan menggunakan fungsi `fopen()`. Kemudian, kode tersebut membaca file baris per baris menggunakan fungsi `fgets()` dan menyimpan isi setiap baris dalam variabel `buffer`.

Pada setiap baris yang dibaca, kode tersebut mencari substring `"MADE "` di dalamnya menggunakan fungsi `strstr()`. Jika substring tersebut ditemukan, maka variabel folder akan menunjuk ke posisi awal dari substring tersebut dalam baris yang sedang dibaca.

Selanjutnya, kode tersebut akan memotong substring `"MADE "` dari variabel folder menggunakan operasi pointer arithmetic dan memberikan nilai `\0` pada karakter terakhir dari substring yang disimpan dalam variabel folder. Ini bertujuan untuk membuang karakter newline `\n` dari variabel folder.

Setelah itu, nilai substring folder akan disalin ke dalam array `folders` dengan menggunakan fungsi `strcpy()`. Variabel `folderCount` kemudian akan diincrement untuk menunjukkan bahwa sebuah folder telah berhasil disalin ke dalam array `folders`.

Dengan demikian, kode tersebut dapat membaca file `"log.txt"` dan menyimpan setiap nama folder yang terdapat pada baris yang mengandung substring `"MADE "`.

Setelah itu dilakukan *sorting* terhadap nama-nama folder dengan bantuan fungsi `sort()` seperti berikut;
```c
void sort(char arr[][500], int n) 
{
    int i, j;
    char temp[100];
    for (i = 0; i < n - 1; i++) 
    {
        for (j = 0; j < n - i - 1; j++) 
        {
            if (strcmp(arr[j], arr[j + 1]) > 0) 
            {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

sort(folders, folderCount);
```
Selanjutnya dilakukan perhitungan file untuk setiap folder dengan menggunakan kode berikut;
```c
fp = fopen("log.txt", "r");
char temp[100];

while (fgets(buffer, 200, fp)) 
{
    if (folder = strstr(buffer, "> ")) 
    {
        folder[strlen(folder) - 2] = '\0';
        strcpy(temp, folder + 2);

        int folderIndex = getFolIndex(temp);

        fileCount[getFolIndex(temp)]++;
    }
}
```
Adapun, untuk melakukan *listing* ekstensi terdapat pada blok kode berikut;
```c
char * extension;

fp = fopen("log.txt", "r");

while (fgets(buffer, 200, fp)) 
{
    if (extension = strstr(buffer, "MOVED ")) 
    {
        strcpy(temp, extension + 6);
        strcpy(extension, temp);

        if (extension[0] == ' ') 
        {
            strcpy(extension, " ");
        } 
        else 
        {
            char * temp2 = strtok(extension, " ");
            strcpy(extension, temp2);
        }

        int extensionExist = 0;

        for (int i = 0; i < totalExt; i++) 
        {
            if (strcmp(extension, extensions[i]) == 0) 
            {
                extensionExist = 1;
            }
        }
        if (extensionExist == 0) 
        {
            strcpy(extensions[totalExt], extension);
            totalExt++;
        }
    }
}
```
Kode tersebut membaca file `"log.txt"` dan mencari setiap baris yang mengandung kata `"MOVED"`. Kemudian kode akan mengekstrak ekstensi file dari baris tersebut dan menyimpannya dalam variabel `"extension"`. Selanjutnya, kode akan memeriksa apakah ekstensi tersebut sudah ada dalam array `"extensions"` yang telah disimpan sebelumnya. Jika belum ada, maka kode akan menambahkan ekstensi baru tersebut ke dalam array `"extensions"`.

Proses pengecekan ekstensi apakah sudah ada atau belum dilakukan dengan cara membandingkan ekstensi yang baru ditemukan dengan setiap elemen dalam array `"extensions"` menggunakan fungsi `strcmp()`. Jika tidak ada satu pun elemen dalam array yang sama dengan ekstensi yang baru ditemukan, maka ekstensi baru tersebut akan disimpan dalam array `"extensions"`.

Berikutnya dilakukan *sorting* terhadap ekstensi yang sudah didapat. Proses *sorting* dilakukan dengan bantuan fungsi `sort()`. Berikut kodenya;
```c
void sort(char arr[][500], int n) 
{
    int i, j;
    char temp[100];
    for (i = 0; i < n - 1; i++) 
    {
        for (j = 0; j < n - i - 1; j++) 
        {
            if (strcmp(arr[j], arr[j + 1]) > 0) 
            {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

sort(extensions, totalExt);
```
Setelah itu, dilakukan proses perhitungan file melalui blok kode berikut;
```c
    fp = fopen("log.txt", "r");

    while ((fgets(buffer, 200, fp))) 
    {
        if (extension = strstr(buffer, "MOVED ")) 
        {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') 
            {
                strcpy(extension, " ");
            } 
            else 
            {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);
            }
            int extensionIndex = getExtIndex(extension);
            extCount[extensionIndex]++;
        }
    }
```

Terakhir, dilakukan output hasil perhitungan file berdasarkan folder dan ekstensi ke terminal. Berikut kodenya;
```c
printf("---   list folder:   ---\n");

for (int i = 1; i < folderCount; i++) 
{
    printf("%s: %d\n", folders[i], fileCount[i]);
}

printf("---   list extension:   ---\n");

for (int i = 0; i < totalExt; i++) 
{
    printf("%s: %d\n", extensions[i], extCount[i]);
}
```

Untuk membantu keseluruhan proses pada program `logchecker.c` ini, terdapat tiga fungsi yang membantu, yaitu;
- fungsi `sort()` yang menerapkan konsep `bubble sort`

- fungsi `getFolIndex()` untuk mendapatkan index folder dari array `folders`.
- fungsi `getExtIndex()` untuk mendapatkan index ekstensi dari array `extensions`.

#### **Output**
**Hasil unzip.c**

![Output_unzip](/uploads/a2e4911261c31fb553cde1b22ae9ac84/Output_unzip.png)

**Hasil categorize.c**

![Output_categ1](/uploads/72728b045fcfec8b1f06ed81a0a7d170/Output_categ1.png)

![Output_categ2](/uploads/8e22454d9ba563ee32a19fd9e9b24a62/Output_categ2.png)

![Output_categ3a](/uploads/6ec1e75926fbf1a8a875e001f2f75211/Output_categ3a.png)

![Output_categ4](/uploads/cd164423afc120fe0894ef543241e0f8/Output_categ4.png)

**Hasil logchecker.c**

![Output_logcheck1](/uploads/fd7a74a532978e4cc587025142da0c7d/Output_logcheck1.png)

![Output_logcheck2](/uploads/295100e0d4d92ba0a38500dcf262e1af/Output_logcheck2.png)

### Kendala
- Masih mengalami kesulitan dalam memahami *multithreading*, terutama dalam hal pencatatan waktu
