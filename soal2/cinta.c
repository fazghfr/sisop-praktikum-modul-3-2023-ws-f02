#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <pthread.h>
#include <time.h>
#include <sys/resource.h>


#define ROW 4
#define COL 5

// Struktur data untuk argumen thread
struct thread_arg {
    int row;
    int col;
    int value;
    unsigned long long *factorial_ptr;
};

// Fungsi thread untuk menghitung faktorial
void *factorial_thread(void *arg) {
    struct thread_arg *t_arg = (struct thread_arg *) arg;
    int num = t_arg->value;
    unsigned long long factorial = 1;
    if (num == 0) {
        factorial = 1;
    } else {
        for (int k = 1; k <= num; k++) {
            factorial *= k;
        }
    }
    *(t_arg->factorial_ptr) = factorial;
    pthread_exit(NULL);
}

int main() {
    int shmid;
    key_t key = 1234;
    int (*result)[COL];

    // Membuat shared memory
    shmid = shmget(key, sizeof(int[ROW][COL]), IPC_CREAT | 0666);

    // Menautkan shared memory ke dalam variabel result
    result = shmat(shmid, NULL, 0);

    // Mengambil hasil perkalian matriks dari shared memory yang telah diisi oleh program kalian.c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // Menghitung faktorial untuk setiap angka pada matriks dengan menggunakan thread
    printf("Hasil faktorial untuk setiap angka pada matriks:\n");
    clock_t start_time = clock();
    unsigned long long factorial_array[ROW * COL];
    int factorial_count = 0;
    pthread_t threads[ROW * COL];
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            struct thread_arg *t_arg = (struct thread_arg *) malloc(sizeof(struct thread_arg));
            t_arg->row = i;
            t_arg->col = j;
            t_arg->value = result[i][j];
            t_arg->factorial_ptr = &factorial_array[factorial_count];
            pthread_create(&threads[factorial_count], NULL, factorial_thread, (void *) t_arg);
            factorial_count++;
        }
    }

    // Join thread
    for (int i = 0; i < factorial_count; i++) {
        pthread_join(threads[i], NULL);
        printf("%llu ", factorial_array[i]);
    }
    printf("\n");

    //Hitung Waktu
    clock_t end_time = clock();
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("\nElapsed time: %f seconds\n", elapsed_time);
    printf("\n");

    //Hitung CPU usage
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    double user_time = (double) usage.ru_utime.tv_sec + ((double) usage.ru_utime.tv_usec / 1000000);
    double system_time = (double) usage.ru_stime.tv_sec + ((double) usage.ru_stime.tv_usec / 1000000);
    double total_time = user_time + system_time;

    double num_cpus = sysconf(_SC_NPROCESSORS_ONLN);
    double cpu_usage_percent = (total_time / num_cpus) * 100;

    printf("CPU usage: %.2f%%\n", cpu_usage_percent);


    // Hitung memory usage statistics
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    long total_memory = pages * page_size;
    long rss_memory = 0;
    FILE *fp = fopen("/home/bilaaripa/Praktikum3/soal3/sisop.c", "r");
    if (fp != NULL) {
        fscanf(fp, "%*s%ld", &rss_memory);
        fclose(fp);
    }
    printf("Total memory: %ld bytes\n", total_memory);
    printf("RSS memory: %ld bytes\n", rss_memory * page_size);

    // Melepaskan shared memory
    if (shmdt(result) == -1) {
        perror("shmdt");
        exit(1);
    }
    if (shmctl(shmid, IPC_RMID, NULL) == -1) {
        perror("shmctl");
        exit(1);
    }

    return 0;
}
