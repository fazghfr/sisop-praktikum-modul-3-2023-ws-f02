#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main()
{
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], hasil[ROW1][COL2];
    int i, j, k, sum;

    srand(time(NULL)); // inisialisasi random seed

    // inisialisasi matriks pertama dengan angka random 1-5
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            matriks1[i][j] = rand() % 5 + 1;
        }
    }

    // inisialisasi matriks kedua dengan angka random 1-4
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            matriks2[i][j] = rand() % 4 + 1;
        }
    }

    // operasi perkalian matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            sum = 0;
            for (k = 0; k < ROW2; k++) {
                sum += matriks1[i][k] * matriks2[k][j];
            }
            hasil[i][j] = sum;
        }
    }

    // membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW1][COL2]), IPC_CREAT | 0666);

    // meng-attach shared memory ke variabel hasilShared
    int (*hasilShared)[COL2] = shmat(shmid, NULL, 0);

    // meng-copy hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            hasilShared[i][j] = hasil[i][j];
        }
    }

    // detaching shared memory
    shmdt(hasilShared);

    // tampilkan matriks hasil
    printf("Hasil perkalian matriks dari program kalian.c\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    return 0;
}
