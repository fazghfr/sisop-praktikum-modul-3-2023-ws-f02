#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/resource.h>


#define ROW 4
#define COL 5

int main() {
    int shmid;
    key_t key = 1234;
    int (*result)[COL];

    // Membuat shared memory
    shmid = shmget(key, sizeof(int[ROW][COL]), IPC_CREAT | 0666);

    // Menautkan shared memory ke dalam variabel result
    result = shmat(shmid, NULL, 0);

    // Mengambil hasil perkalian matriks dari shared memory yang telah diisi oleh program kalian.c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // Menghitung faktorial untuk setiap angka pada matriks tanpa menggunakan thread
    printf("Hasil faktorial untuk setiap angka pada matriks:\n");
    clock_t start_time = clock();
    unsigned long long factorials[ROW * COL];
    int index = 0;
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            int num = result[i][j];
            unsigned long long factorial = 1;
            if (num == 0) {
                factorial = 1;
            } else {
                for (int k = 1; k <= num; k++) {
                    factorial *= k;
                }
            }
            factorials[index++] = factorial;
        }
    }
    for (int i = 0; i < ROW * COL; i++) {
        printf("%llu ", factorials[i]);
    }
    printf("\n");

    //Hitung Waktu
    clock_t end_time = clock();
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("\nElapsed time: %f seconds\n", elapsed_time);
    printf("\n");

    //Hitung CPU usage
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    double user_time = (double) usage.ru_utime.tv_sec + ((double) usage.ru_utime.tv_usec / 1000000);
    double system_time = (double) usage.ru_stime.tv_sec + ((double) usage.ru_stime.tv_usec / 1000000);
    double total_time = user_time + system_time;

    double num_cpus = sysconf(_SC_NPROCESSORS_ONLN);
    double cpu_usage_percent = (total_time / num_cpus) * 100;

    printf("CPU usage: %.2f%%\n", cpu_usage_percent);

    // Hitung memory usage statistics
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    long total_memory = pages * page_size;
    long rss_memory = 0;
    FILE *fp = fopen("/home/bilaaripa/Praktikum3/soal3/sisop.c", "r");
    if (fp != NULL) {
        fscanf(fp, "%*s%ld", &rss_memory);
        fclose(fp);
    }
    printf("Total memory: %ld bytes\n", total_memory);
    printf("RSS memory: %ld bytes\n", rss_memory * page_size);

    // Melepaskan shared memory
    if (shmdt(result) == -1) {
        perror("shmdt");
        exit(1);
    }
    if (shmctl(shmid, IPC_RMID, NULL) == -1) {
        perror("shmctl");
        exit(1);
    }

    return 0;
}
 